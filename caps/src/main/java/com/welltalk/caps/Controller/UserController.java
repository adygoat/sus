package com.welltalk.caps.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.welltalk.caps.Entity.UserEntity;
import com.welltalk.caps.Repository.UserRepository;
import com.welltalk.caps.Service.UserService;

	@RestController
	@CrossOrigin(origins = "http://localhost:19006")
	public class UserController {

	    @Autowired
	    private UserService userService; // Autowire the UserRepository

	    @PostMapping("/signup")
	    public ResponseEntity<String> signup(@RequestBody UserEntity user) {
	        return userService.signup(user); // Call the non-static method on the instance
	    }
	    
	    @GetMapping("/user/{userid}")
	    public ResponseEntity<UserEntity> getUserByUserId(@PathVariable Long userid) {
	        // Call the service method to fetch user data by userid
	        UserEntity user = userService.getUserByUserId(userid);
	        if (user != null) {
	            return ResponseEntity.ok(user);
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    }
	    
	    }