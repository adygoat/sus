package com.welltalk.caps.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.welltalk.caps.Entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
	boolean existsByEmail(String email);
	boolean existsByUserid(Long userid);
	UserEntity findByUserid(Long userid);
}
