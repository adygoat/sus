package com.welltalk.caps.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.welltalk.caps.Entity.AppointmentEntity;

public interface AppointmentRepository extends JpaRepository<AppointmentEntity, Integer> {


}
